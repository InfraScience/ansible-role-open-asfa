---
- name: Create the compose directory
  run_once: true
  when: docker_swarm_manager_main_node is defined and docker_swarm_manager_main_node | bool
  tags: ['open_asfa', 'open_asfa_swarm']
  block:
    - name: Create the compose directory
      ansible.builtin.file:
        dest: "{{ open_asfa_compose_dir }}"
        state: directory
        owner: root
        group: root
        mode: 0700

- name: Manage the installation of the OpenASFA configuration of the database
  when:
    - open_asfa_db_docker_host == ansible_fqdn
    - open_asfa_db_as_container
    - open_asfa_pg_volume_type == "local"
  tags: ['open_asfa', 'open_asfa_swarm', 'open_asfa_db']
  run_once: true
  block:
    - name: Create the directory where the DB init script is going to be installed
      ansible.builtin.file:
        dest: "{{ open_asfa_compose_dir }}"
        state: directory
        owner: root
        group: root
        mode: 0700

    - name: Install the DB initialization script
      ansible.builtin.template:
        src: pg-create-user-db.sh.j2
        dest: "{{ open_asfa_compose_dir }}/pg-create-user-db.sh"
        owner: root
        group: root
        mode: 0555

    - name: Add the label that will be used as a constraint for the PostgreSQL DB
      community.docker.docker_node:
        hostname: '{{ open_asfa_db_docker_host }}'
        labels:
          pg_data_asfa: 'asfa_server'
          pgadmin_data_asfa: 'asfa_server'
        labels_state: 'merge'

- name: Manage the installation of the OpenASFA configuration of the swarm service
  when: docker_swarm_manager_main_node is defined and docker_swarm_manager_main_node | bool
  tags: ['open_asfa', 'open_asfa_swarm']
  run_once: true
  block:
    - name: Install the docker compose file for postgresql and pgadmin
      ansible.builtin.template:
        src: open-asfa-db-docker-compose.yml.j2
        dest: "{{ open_asfa_compose_dir }}/docker-open-asfa-stack-db.yml"
        owner: root
        group: root
        mode: 0400

    - name: Install the docker compose file of Couchbase
      ansible.builtin.template:
        src: open-asfa-couchbase-docker-compose.yml.j2
        dest: "{{ open_asfa_compose_dir }}/docker-open-asfa-stack-couchbase.yml"
        owner: root
        group: root
        mode: 0400

    - name: Install the docker compose file of the ASFA service
      ansible.builtin.template:
        src: open-asfa-docker-compose.yml.j2
        dest: "{{ open_asfa_compose_dir }}/docker-open-asfa-stack.yml"
        owner: root
        group: root
        mode: 0400

    - name: Install the DB initialization script
      ansible.builtin.template:
        src: pg-create-user-db.sh.j2
        dest: "{{ open_asfa_compose_dir }}/pg-create-user-db.sh"
        owner: root
        group: root
        mode: 0555
      when:
        - open_asfa_db_as_container
        - open_asfa_pg_volume_type != "local"

    - name: Create the secret for the Postgres initizalization script
      community.docker.docker_secret:
        name: open_asfa_pg_config
        data_src: '{{ open_asfa_compose_dir }}/pg-create-user-db.sh'
        state: present

    - name: Install the pgadmin configuration files
      ansible.builtin.template:
        src: '{{ open_asfa_pgadmin_config_item }}.j2'
        dest: '{{ open_asfa_compose_dir }}/{{ open_asfa_pgadmin_config_item }}'
        owner: root
        group: root
        mode: 0444
      loop:
        - pgadmin_config_local.py
        - pgadmin_servers.json
      loop_control:
        loop_var: open_asfa_pgadmin_config_item

    - name: Create the secret for the pgadmin config local
      community.docker.docker_secret:
        name: open_asfa_pgadmin_config
        data_src: '{{ open_asfa_compose_dir }}/pgadmin_config_local.py'
        state: present

    - name: Create the secret for the pgadmin config servers
      community.docker.docker_secret:
        name: open_asfa_pgadmin_servers
        data_src: '{{ open_asfa_compose_dir }}/pgadmin_servers.json'
        state: present

    - name: Start the OpenAsfa stack
      community.docker.docker_stack:
        name: open-asfa
        state: present
        compose:
          - '{{ open_asfa_compose_dir }}/docker-open-asfa-stack-db.yml'
          - '{{ open_asfa_compose_dir }}/docker-open-asfa-stack-couchbase.yml'
          - '{{ open_asfa_compose_dir }}/docker-open-asfa-stack.yml'
