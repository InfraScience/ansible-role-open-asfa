Role Name
=========

A role that installs the Open ASFA docker stack

Role Variables
--------------

The most important variables are listed below:

``` yaml
open_asfa_compose_dir: '/srv/open_asfa_stack'
open_asfa_docker_stack_name: 'open-asfa'
open_asfa_docker_service_server_name: 'asfa-server'
open_asfa_docker_service_client_name: 'asfa-client'
open_asfa_docker_server_image: 'lucabrl01/asfa-server'
open_asfa_docker_client_image: 'lucabrl01/asfa-client'
open_asfa_docker_network: 'open_asfa_net'
# IMPORTANT. Set it to True for the server that is going to host the DB
open_asfa_docker_db_node: False
open_asfa_behind_haproxy: True
open_asfa_haproxy_public_net: 'haproxy-public'
# DB
open_asfa_db_image: 'postgres:12-alpine'
#open_asfa_db_pwd: 'set it in a vault file'
open_asfa_db_name: 'asfadb'
open_asfa_db_user: 'asfadb_user'
open_asfa_db_volume: 'asfa_pg_data'
```

Dependencies
------------

A docker swarm cluster is required

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
